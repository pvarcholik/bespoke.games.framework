#pragma once

#include "Game.h"

namespace Library
{
	class KeyboardComponent;
}

namespace BouncingBall
{
	class Ball;

	class BouncingBallGame : public Library::Game
	{
	public:
		BouncingBallGame(std::function<void*()> getWindowCallback, std::function<void(SIZE&)> getRenderTargetSizeCallback);

		virtual void Initialize() override;
		virtual void Shutdown() override;
		virtual void Update(const Library::GameTime& gameTime) override;
		virtual void Draw(const Library::GameTime& gameTime) override;

	private:
		void Exit();

		static const DirectX::XMVECTORF32 BackgroundColor;

		std::shared_ptr<Library::KeyboardComponent> mKeyboard;
		std::shared_ptr<Ball> mBall;
	};
}