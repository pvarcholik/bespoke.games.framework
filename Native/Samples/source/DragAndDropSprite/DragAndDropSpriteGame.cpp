#include "pch.h"
#include "DragAndDropSpriteGame.h"

using namespace std;
using namespace DirectX;
using namespace Library;
using namespace Microsoft::WRL;

namespace DragAndDropSprite
{
	const XMVECTORF32 DragAndDropSpriteGame::BackgroundColor = Colors::Black;
	const XMFLOAT2 DragAndDropSpriteGame::HelpTextPosition = { 0.0f, 22.0f };

	DragAndDropSpriteGame::DragAndDropSpriteGame(function<void*()> getWindowCallback, function<void(SIZE&)> getRenderTargetSizeCallback) :
		Game(getWindowCallback, getRenderTargetSizeCallback), mSpriteBounds(Rectangle::Empty)
	{
	}

	void DragAndDropSpriteGame::Initialize()
	{
		SpriteManager::Initialize(*this);

		mKeyboard = make_shared<KeyboardComponent>(*this);
		mComponents.push_back(mKeyboard);
		mServices.AddService(KeyboardComponent::TypeIdClass(), mKeyboard.get());

		mMouse = make_shared<MouseComponent>(*this, MouseModes::Absolute);
		mComponents.push_back(mMouse);
		mServices.AddService(MouseComponent::TypeIdClass(), mMouse.get());

		mFont = make_shared<SpriteFont>(mDirect3DDevice.Get(), L"Content\\Fonts\\Arial_14_Regular.spritefont");

		// Load a texture
		ComPtr<ID3D11Resource> textureResource;
		wstring textureName = L"Content\\Textures\\BlockWood_beige_size64.png";

		ThrowIfFailed(CreateWICTextureFromFile(Direct3DDevice(), textureName.c_str(), textureResource.ReleaseAndGetAddressOf(), mSpriteTexture.ReleaseAndGetAddressOf()), "CreateWICTextureFromFile() failed.");

		ComPtr<ID3D11Texture2D> texture;
		ThrowIfFailed(textureResource.As(&texture), "Invalid ID3D11Resource returned from CreateWICTextureFromFile. Should be a ID3D11Texture2D.");

		mSpriteBounds = TextureHelper::GetTextureBounds(texture.Get());
		Point textureHalfSize(mSpriteBounds.Width / 2, mSpriteBounds.Height / 2);

		Library::Rectangle viewportSize(static_cast<int>(mViewport.TopLeftX), static_cast<int>(mViewport.TopLeftY), static_cast<int>(mViewport.Width), static_cast<int>(mViewport.Height));
		Point center = viewportSize.Center();
		mSpriteBounds.X = center.X - textureHalfSize.X;
		mSpriteBounds.Y = center.Y - textureHalfSize.Y;

		Game::Initialize();
	}

	void DragAndDropSpriteGame::Shutdown()
	{
		SpriteManager::Shutdown();

		Game::Shutdown();
	}

	void DragAndDropSpriteGame::Update(const GameTime &gameTime)
	{
		if (mKeyboard->WasKeyPressedThisFrame(Keys::Escape))
		{
			Exit();
		}

		if (mMouse->WasButtonPressedThisFrame(MouseButtons::Left) && !mIsSpriteDragging)
		{
			const int hitBoxWidth = 8;
			const int hitBoxHeight = 8;

			Library::Rectangle mouseBounds(mMouse->X(), mMouse->Y(), hitBoxWidth, hitBoxHeight);
			if (mSpriteBounds.Intersects(mouseBounds))
			{
				mSpriteDraggingOffset.X = mSpriteBounds.X - mMouse->X();
				mSpriteDraggingOffset.Y = mSpriteBounds.Y - mMouse->Y();
				mIsSpriteDragging = true;
			}
		}

		if (mMouse->WasButtonReleasedThisFrame(MouseButtons::Left))
		{
			mIsSpriteDragging = false;
		}

		if (mIsSpriteDragging)
		{
			mSpriteBounds.X = mMouse->X() + mSpriteDraggingOffset.X;
			mSpriteBounds.Y = mMouse->Y() + mSpriteDraggingOffset.Y;
		}

		Game::Update(gameTime);
	}

	void DragAndDropSpriteGame::Draw(const GameTime &gameTime)
	{
		mDirect3DDeviceContext->ClearRenderTargetView(mRenderTargetView.Get(), reinterpret_cast<const float*>(&BackgroundColor));
		mDirect3DDeviceContext->ClearDepthStencilView(mDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
				
		Game::Draw(gameTime);

		wostringstream mousePositionText;
		mousePositionText << "Left-Click Sprite to Drag\nMouse (x, y): " << mMouse->X() << ", " << mMouse->Y();
		SpriteManager::DrawString(mFont, mousePositionText.str().c_str(), HelpTextPosition);

		XMFLOAT2 position(static_cast<float>(mSpriteBounds.X), static_cast<float>(mSpriteBounds.Y));
		SpriteManager::DrawTexture2D(mSpriteTexture.Get(), position);

		HRESULT hr = mSwapChain->Present(1, 0);

		// If the device was removed either by a disconnection or a driver upgrade, we must recreate all device resources.
		if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
		{
			HandleDeviceLost();
		}
		else
		{
			ThrowIfFailed(hr, "IDXGISwapChain::Present() failed.");
		}
	}

	void DragAndDropSpriteGame::Exit()
	{
		PostQuitMessage(0);
	}
}