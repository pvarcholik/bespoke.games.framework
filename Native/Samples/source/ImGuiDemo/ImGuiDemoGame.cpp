#include "pch.h"
#include "ImGuiDemoGame.h"
#include <ImGui\imgui_impl_dx11.h>

using namespace std;
using namespace DirectX;
using namespace Library;
using namespace Microsoft::WRL;

IMGUI_API LRESULT ImGui_ImplDX11_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

namespace ImGuiDemo
{
	XMVECTORF32 ImGuiDemoGame::BackgroundColor = Colors::Black;

	ImGuiDemoGame::ImGuiDemoGame(function<void*()> getWindowCallback, function<void(SIZE&)> getRenderTargetSizeCallback) :
		Game(getWindowCallback, getRenderTargetSizeCallback), mShowTestWindow(true), mShowAnotherWindow(false)
	{
	}

	void ImGuiDemoGame::Initialize()
	{
		SpriteManager::Initialize(*this);
		BlendStates::Initialize(mDirect3DDevice.Get());

		mKeyboard = make_shared<KeyboardComponent>(*this);
		mComponents.push_back(mKeyboard);
		mServices.AddService(KeyboardComponent::TypeIdClass(), mKeyboard.get());

		auto mouse = make_shared<MouseComponent>(*this);
		mComponents.push_back(mouse);
		mServices.AddService(MouseComponent::TypeIdClass(), mouse.get());

		mImGui = make_shared<ImGuiComponent>(*this);
		mComponents.push_back(mImGui);
		mServices.AddService(ImGuiComponent::TypeIdClass(), mImGui.get());	
		auto imGuiWndProcHandler = make_shared<UtilityWin32::WndProcHandler>(ImGui_ImplDX11_WndProcHandler);
		UtilityWin32::AddWndProcHandler(imGuiWndProcHandler);

		// 1. Show a simple window
		// Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug"
		auto sampleImGuiRenderBlock1 = make_shared<ImGuiComponent::RenderBlock>([this]()
		{					
			static float f = 0.0f;
			ImGui::Text("Hello, world!");
			ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
			ImGui::ColorEdit3("clear color", reinterpret_cast<float*>(&BackgroundColor));
			if (ImGui::Button("Test Window")) mShowTestWindow ^= 1;
			if (ImGui::Button("Another Window")) mShowAnotherWindow ^= 1;
			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		});
		mImGui->AddRenderBlock(sampleImGuiRenderBlock1);

		// 2. Show another simple window, this time using an explicit Begin/End pair
		auto sampleImGuiRenderBlock2 = make_shared<ImGuiComponent::RenderBlock>([this]()
		{
			if (mShowAnotherWindow)
			{
				ImGui::Begin("Another Window", &mShowAnotherWindow);
				ImGui::Text("Hello from another window!");
				ImGui::End();
			}
		});
		mImGui->AddRenderBlock(sampleImGuiRenderBlock2);
				
		// 3. Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
		auto sampleImGuiRenderBlock3 = make_shared<ImGuiComponent::RenderBlock>([this]()
		{
			if (mShowTestWindow)
			{
				ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiCond_FirstUseEver);     // Normally user code doesn't need/want to call it because positions are saved in .ini file anyway. Here we just want to make the demo initial state a bit more friendly!
				ImGui::ShowTestWindow(&mShowTestWindow);
			}
		});
		mImGui->AddRenderBlock(sampleImGuiRenderBlock3);

		Game::Initialize();
	}

	void ImGuiDemoGame::Shutdown()
	{
		BlendStates::Shutdown();
		SpriteManager::Shutdown();

		Game::Shutdown();
	}

	void ImGuiDemoGame::Update(const GameTime &gameTime)
	{
		if (mKeyboard->WasKeyPressedThisFrame(Keys::Escape))
		{
			Exit();
		}

		Game::Update(gameTime);
	}

	void ImGuiDemoGame::Draw(const GameTime &gameTime)
	{
		mDirect3DDeviceContext->ClearRenderTargetView(mRenderTargetView.Get(), reinterpret_cast<const float*>(&BackgroundColor));
		mDirect3DDeviceContext->ClearDepthStencilView(mDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
				
		Game::Draw(gameTime);

		HRESULT hr = mSwapChain->Present(1, 0);

		// If the device was removed either by a disconnection or a driver upgrade, we must recreate all device resources.
		if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
		{
			HandleDeviceLost();
		}
		else
		{
			ThrowIfFailed(hr, "IDXGISwapChain::Present() failed.");
		}
	}

	void ImGuiDemoGame::Exit()
	{
		PostQuitMessage(0);
	}
}