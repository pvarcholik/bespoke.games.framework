#pragma once

#include "Game.h"

namespace Library
{
	class KeyboardComponent;
	class MouseComponent;	
}

namespace DirectX
{
	class SpriteFont;
	class SoundEffect;
	class SoundEffectInstance;
}

namespace AudioDemo
{

	class AudioDemoGame : public Library::Game
	{
	public:
		AudioDemoGame(std::function<void*()> getWindowCallback, std::function<void(SIZE&)> getRenderTargetSizeCallback);

		virtual void Initialize() override;
		virtual void Shutdown() override;
		virtual void Update(const Library::GameTime& gameTime) override;
		virtual void Draw(const Library::GameTime& gameTime) override;

	private:
		void Exit();

		static const DirectX::XMVECTORF32 BackgroundColor;
		static const std::wstring HelpText;
		static const DirectX::XMFLOAT2 HelpTextPosition;

		std::shared_ptr<Library::KeyboardComponent> mKeyboard;
		std::shared_ptr<Library::MouseComponent> mMouse;
		std::shared_ptr<DirectX::SpriteFont> mFont;		

		std::unique_ptr<DirectX::SoundEffect> mLoopingSoundEffect;
		std::unique_ptr<DirectX::SoundEffectInstance> mLoopingSound;
		std::unique_ptr<DirectX::SoundEffect> mRightClickSoundEffect;
	};
}