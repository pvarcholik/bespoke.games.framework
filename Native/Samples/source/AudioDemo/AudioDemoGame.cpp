#include "pch.h"
#include "AudioDemoGame.h"

using namespace std;
using namespace DirectX;
using namespace Library;
using namespace Microsoft::WRL;

namespace AudioDemo
{
	const XMVECTORF32 AudioDemoGame::BackgroundColor = Colors::Black;
	const XMFLOAT2 AudioDemoGame::HelpTextPosition = { 0.0f, 22.0f };
	const wstring AudioDemoGame::HelpText = L"Play Looping Sound (Left Mouse)\nStop Looping Sound (Middle Mouse)\nPlay Single Sound (Right Mouse)";

	AudioDemoGame::AudioDemoGame(function<void*()> getWindowCallback, function<void(SIZE&)> getRenderTargetSizeCallback) :
		Game(getWindowCallback, getRenderTargetSizeCallback)
	{
	}

	void AudioDemoGame::Initialize()
	{
		SpriteManager::Initialize(*this);

		auto audioEngine = make_shared<AudioEngineComponent>(*this);
		mComponents.push_back(audioEngine);
		mServices.AddService(AudioEngineComponent::TypeIdClass(), audioEngine.get());

		mLoopingSoundEffect = make_unique<SoundEffect>(audioEngine->AudioEngine().get(), L"Content\\Audio\\Pew.wav");
		mLoopingSound = mLoopingSoundEffect->CreateInstance();

		mRightClickSoundEffect = make_unique<SoundEffect>(audioEngine->AudioEngine().get(), L"Content\\Audio\\Pew2.wav");

		mKeyboard = make_shared<KeyboardComponent>(*this);
		mComponents.push_back(mKeyboard);
		mServices.AddService(KeyboardComponent::TypeIdClass(), mKeyboard.get());

		mMouse = make_shared<MouseComponent>(*this);
		mComponents.push_back(mMouse);
		mServices.AddService(MouseComponent::TypeIdClass(), mMouse.get());

		mFont = make_shared<SpriteFont>(mDirect3DDevice.Get(), L"Content\\Fonts\\Arial_14_Regular.spritefont");

		Game::Initialize();
	}

	void AudioDemoGame::Shutdown()
	{
		SpriteManager::Shutdown();

		Game::Shutdown();
	}

	void AudioDemoGame::Update(const GameTime &gameTime)
	{
		if (mKeyboard->WasKeyPressedThisFrame(Keys::Escape))
		{
			Exit();
		}

		if (mMouse->WasButtonPressedThisFrame(MouseButtons::Left))
		{
			mLoopingSound->Play(true);			
		}

		if (mMouse->WasButtonPressedThisFrame(MouseButtons::Middle))
		{
			mLoopingSound->Stop();
		}

		if (mMouse->WasButtonPressedThisFrame(MouseButtons::Right))
		{
			mRightClickSoundEffect->Play();
		}

		Game::Update(gameTime);
	}

	void AudioDemoGame::Draw(const GameTime &gameTime)
	{
		mDirect3DDeviceContext->ClearRenderTargetView(mRenderTargetView.Get(), reinterpret_cast<const float*>(&BackgroundColor));
		mDirect3DDeviceContext->ClearDepthStencilView(mDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
				
		Game::Draw(gameTime);

		SpriteManager::DrawString(mFont, HelpText.c_str(), HelpTextPosition);

		HRESULT hr = mSwapChain->Present(1, 0);

		// If the device was removed either by a disconnection or a driver upgrade, we must recreate all device resources.
		if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
		{
			HandleDeviceLost();
		}
		else
		{
			ThrowIfFailed(hr, "IDXGISwapChain::Present() failed.");
		}
	}

	void AudioDemoGame::Exit()
	{
		PostQuitMessage(0);
	}
}