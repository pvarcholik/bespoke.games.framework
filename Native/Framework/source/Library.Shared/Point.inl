namespace Library
{
	inline Point Point::operator+(const Point& rhs) const
	{
		return Point(X + rhs.X, Y + rhs.Y);
	}

	inline bool Point::operator<(const Point& rhs) const
	{
		return (X == rhs.X ? Y < rhs.Y : X < rhs.X);
	}

	inline Point& Point::operator+=(const Point& rhs)
	{
		X += rhs.X;
		Y += rhs.Y;
		return *this;
	}
}