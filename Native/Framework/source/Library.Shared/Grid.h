#pragma once

#include "DrawableGameComponent.h"
#include <wrl.h>
#include <d3d11_2.h>
#include <DirectXMath.h>

namespace Library
{
	class BasicMaterial;

	class Grid final : public DrawableGameComponent
	{
		RTTI_DECLARATIONS(Grid, DrawableGameComponent)

	public:
		explicit Grid(Game& game, const std::shared_ptr<Camera>& camera, std::uint32_t size = DefaultSize, std::uint32_t scale = DefaultScale, const DirectX::XMFLOAT4& color = DefaultColor);
		Grid(const Grid&) = delete;
		Grid(Grid&&) = default;
		Grid& operator=(const Grid&) = delete;
		Grid& operator=(Grid&&) = default;
		~Grid() = default;

		const DirectX::XMFLOAT3& Position() const;
		void SetPosition(const DirectX::XMFLOAT3& position);
		void SetPosition(float x, float y, float z);

		const DirectX::XMFLOAT4 Color() const;
		void SetColor(const DirectX::XMFLOAT4& color);

		const std::uint32_t Size() const;
		void SetSize(std::uint32_t size);

		const std::uint32_t Scale() const;
		void SetScale(std::uint32_t scale);

		virtual void Initialize() override;
		virtual void Draw(const GameTime& gameTime) override;

	private:
		void InitializeGrid();
		void UpdateMaterial();

		static const std::uint32_t DefaultSize;
		static const std::uint32_t DefaultScale;
		static const DirectX::XMFLOAT4 DefaultColor;

		Microsoft::WRL::ComPtr<ID3D11Buffer> mVertexBuffer;
		std::shared_ptr<BasicMaterial> mMaterial;
		DirectX::XMFLOAT3 mPosition;
		std::uint32_t mSize;
		std::uint32_t mScale;
		DirectX::XMFLOAT4 mColor;
		DirectX::XMFLOAT4X4 mWorldMatrix;
	};
}